import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';



@Component({
  selector: 'app-pc',
  templateUrl: './pc.component.html',
  styleUrls: ['./pc.component.css']
})
export class PcComponent implements OnInit {

  forma!: FormGroup;


  constructor(private fb: FormBuilder) {
    this.crearFormulario();
    this.cargarDataAlFomulario2();
    
  }


  ngOnInit(): void {
  }

  crearFormulario(): void {
    this.forma = this.fb.group({
      //Valores del array:
      //1er valor: El valor por defecto que tendra
      //2do valor: Son los validadores sincronos
      //3er valor: Son los validadores asincronos
      nombre: ['', [Validators.required, Validators.minLength(4)]],
      apellido: ['', [Validators.required, Validators.minLength(4)]],
      correo: ['', [Validators.required, Validators.pattern(/^(([^<>()\[\]\.,;:\s@\”]+(\.[^<>()\[\]\.,;:\s@\”]+)*)|(\”.+\”))@(([^<>()[\]\.,;:\s@\”]+\.)+[^<>()[\]\.,;:\s@\”]{2,})$/)]],
      pass1: ['', Validators.required],
       pass2: ['', Validators.required],
    })
  };

  get nombreNoValido() {
    return this.forma.get('nombre')?.invalid && this.forma.get('nombre')?.touched;
  }

  get apellidoNoValido() {
    return this.forma.get('apellido')?.invalid && this.forma.get('apellido')?.touched;
  }

  get correoNoValido() {
    return this.forma.get('correo')?.invalid && this.forma.get('correo')?.touched;
  }

  cargarDataAlFomulario(): void {
    this.forma.setValue({
      nombre: '',
      apellido: '',
      correo: '',
    })
  }

  guardar(): void{
    if(!this.forma.valid){
      return;
    }

    console.log(this.forma.value);
    //Reset del formulario
    this.LimpiarFomulario();
  }

  LimpiarFomulario(): void{
    //this.forma.reset();
    this.forma.reset({
      nombre: 'Pedro'
    });


}
cargarDataAlFomulario2(): void{
  this.forma.patchValue({
    apellido: 'Perez'
  });
}

get pass1Novalido(){
  return this.forma.get('pass1')?.invalid && this.forma.get('pass1')?.touched;
}

get pass2Novalido(){
  const pass1 = this.forma.get('pass1')?.value;
  const pass2 = this.forma.get('pass2')?.value;
  
  return this.forma.get('pass2')?.hasError('noEsIgual');
}
}






