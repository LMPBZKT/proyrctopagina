import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Lateral3Component } from './lateral3.component';

describe('Lateral3Component', () => {
  let component: Lateral3Component;
  let fixture: ComponentFixture<Lateral3Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Lateral3Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Lateral3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
