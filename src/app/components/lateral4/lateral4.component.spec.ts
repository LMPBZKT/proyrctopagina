import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Lateral4Component } from './lateral4.component';

describe('Lateral4Component', () => {
  let component: Lateral4Component;
  let fixture: ComponentFixture<Lateral4Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Lateral4Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Lateral4Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
