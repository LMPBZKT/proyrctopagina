import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PcComponent } from './components/pc/pc.component';

const routes: Routes = [
  { path: 'pc', component: PcComponent },
  { path: '**', pathMatch: 'full', redirectTo: 'pc' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
